from django.views.generic import TemplateView
from django.shortcuts import render, render_to_response, get_object_or_404
import datapoliceuk.rest2model as rest2model


# Create your views here.
class Dashboard(TemplateView):

    def get(self, request, *args, **kwargs):
        data = rest2model.get_crimes_street_dates()
        return render(request, 'dashboard.html', {'data': data})
