import requests

import json
import os
from datapoliceuk.models import CrimesStreetDates

from rest_framework import serializers

from django.utils.six import BytesIO
from rest_framework.parsers import JSONParser


class CustomJSONParser(JSONParser):

    def parse(self, stream, media_type=None, parser_context=None):
        '''
        Workaround the problem with Django Rest Framework not properly handling field names with hyphens as described
        here: https://github.com/encode/django-rest-framework/issues/967

        :param stream:
        :param media_type:
        :param parser_context:
        :return: Same list of JSON objects with underscores instead of hyphens
        '''
        json_list = super().parse(stream, media_type, parser_context)

        fixed_list = []
        while json_list:
            json_object = json_list.pop()
            fixed_json_object = dict()
            for key in json_object:
                fixed_json_object[key.replace('-', '_')] = json_object[key]
            fixed_list.append(fixed_json_object)
            del json_object
        return fixed_list


class ForcesSerializer(serializers.Serializer):
    #id = serializers.CharField(max_length=200, primary_key=True)
    name = serializers.CharField(max_length=200, default="")


class CrimesStreetDatesSerializer(serializers.Serializer):
    date = serializers.DateField(input_formats=["%Y-%m"])
    stop_and_search = serializers.ListField( allow_empty=True)

    class Meta:
        model = CrimesStreetDates
        fields = ('date', 'stop_and_search')


datapoliceuk_url = "https://data.police.uk/api/"


def get_crimes_street_dates():
    url = datapoliceuk_url + "crimes-street-dates"
    r = requests.get(url)

    stream = BytesIO(r.text.encode())
    data = CustomJSONParser().parse(stream)



    serializer = CrimesStreetDatesSerializer(data=data, many=True)
    try:
        serializer.is_valid(raise_exception=True)
        print("Validated data")
        return serializer.validated_data

    except Exception as e:
        print(str(e))


#get_crimes_street_dates()