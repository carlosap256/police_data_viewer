from django.db import models

# Create your models here.


class Forces(models.Model):
    id = models.CharField(max_length=200, primary_key=True)
    name = models.CharField(max_length=200, default="")


class EngagementMethods(models.Model):
    url = models.URLField(null=True)
    description = models.TextField(default="")
    title = models.CharField(default="", max_length=200)


class SpecificForce(models.Model):
    description = models.TextField(default="")
    url = models.URLField(null=True)
    engagement_methods = models.ForeignKey(EngagementMethods, on_delete=models.CASCADE)
    telephone = models.CharField(default="", max_length=200)
    id = models.OneToOneField(Forces, on_delete=models.CASCADE, primary_key=True)
    name = models.CharField(default="", max_length=200)


class ContactDetails(models.Model):
    email = models.CharField(default="", max_length=200)
    telephone = models.CharField(default="", max_length=200)
    mobile = models.CharField(default="", max_length=200)
    fax = models.CharField(default="", max_length=200)
    web = models.URLField(null=True)
    address = models.CharField(default="", max_length=200)
    facebook = models.URLField(null=True)
    twitter = models.URLField(null=True)
    youtube = models.URLField(null=True)
    myspace = models.URLField(null=True)
    bebo = models.URLField(null=True)
    flickr = models.URLField(null=True)
    google_plus = models.URLField(null=True)
    forum = models.URLField(null=True)
    e_messaging = models.URLField(null=True)
    blog = models.URLField(null=True)
    rss = models.URLField(null=True)


class SeniorOffices(models.Model):
    bio = models.TextField(default="")
    contact_details = models.ForeignKey(ContactDetails, on_delete=models.CASCADE)
    name = models.CharField(default="", max_length=200)
    rank = models.CharField(default="", max_length=200)


class CrimesStreetDates(models.Model):

    date = models.DateField()
    stop_and_search = models.ManyToManyField(Forces, blank=True)

