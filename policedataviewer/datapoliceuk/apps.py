from django.apps import AppConfig


class DatapoliceukConfig(AppConfig):
    name = 'datapoliceuk'
