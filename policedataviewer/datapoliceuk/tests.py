from django.test import TestCase

# Create your tests here.
from datapoliceuk.models import CrimesStreetDates

import os
from datetime import datetime
from datapoliceuk.rest2model import CustomJSONParser
from django.utils.six import BytesIO

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def get_json_from_file(filename):
    f = os.path.join(BASE_DIR, 'datapoliceuk', 'resources', 'testdata', filename)
    try:
        with open(f, 'r') as myfile:
            stream = BytesIO(myfile.read().encode())
            return CustomJSONParser().parse(stream)
    except Exception as e:
        print(str(e))

    return dict()


class CrimesStreetDatesTestCase(TestCase):

    def test_from_json_file(self):

        json_object = get_json_from_file('crimes-street-dates.json')
        self.assertEqual(len(json_object), 2)